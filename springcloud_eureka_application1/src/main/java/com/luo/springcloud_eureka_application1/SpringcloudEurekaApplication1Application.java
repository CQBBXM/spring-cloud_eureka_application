package com.luo.springcloud_eureka_application1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.luo.springcloud_eureka_application1.feign"})
public class SpringcloudEurekaApplication1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudEurekaApplication1Application.class, args);
    }
}
