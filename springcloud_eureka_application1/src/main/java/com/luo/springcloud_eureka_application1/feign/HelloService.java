package com.luo.springcloud_eureka_application1.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * AUTHORED BY luohuanran
 * DATE: 2018/11/16 16:05
 */
@FeignClient(name = "springbootApplication2")
public interface HelloService {

    @GetMapping("/hello")
    String hello();
}
