package com.luo.springcloud_eureka_application1.controller;

import com.luo.springcloud_eureka_application1.feign.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AUTHORED BY luohuanran
 * DATE: 2018/11/16 16:04
 */
@RestController
@RefreshScope
public class TestController {

    @Value("${neo.hello}")
    private String hello;

    @Autowired
    private HelloService helloService;

    @GetMapping("/test")
    public String test(){
        return helloService.hello();
    }

    @GetMapping("/hello")
    public String hello(){
        return this.hello;
    }
}
