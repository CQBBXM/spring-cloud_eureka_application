package com.luo.springcloud_eureka_application2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AUTHORED BY luohuanran
 * DATE: 2018/11/16 16:08
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "hello are you ok!";
    }
}
