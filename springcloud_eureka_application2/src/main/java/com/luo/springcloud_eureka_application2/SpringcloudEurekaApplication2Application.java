package com.luo.springcloud_eureka_application2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
public class SpringcloudEurekaApplication2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudEurekaApplication2Application.class, args);
    }
}
